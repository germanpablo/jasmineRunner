var gulp = require('gulp');
var Server = require('karma').Server;

gulp.task('copy', function() {
   gulp.src('PATH')
   .pipe(gulp.dest('./script'));
});


gulp.task('test', function(done) {
    Server.start({
        configFile: __dirname + '/karma.conf.js',
        singleRun: true
    }, function() {
        done();
    });
});
module.exports = function(config) {
  config.set({    
    basePath: 'script/',
    frameworks: ['jasmine'],	   
    files: [],
    exclude: [],
    preprocessors: {},
    reporters: ['mocha'],
    port: 9876,
    colors: true,
    logLevel: config.LOG_INFO,
    autoWatch: false,
    browsers: ['PhantomJS'],
    singleRun: true,
    concurrency: Infinity,	
  })
}
